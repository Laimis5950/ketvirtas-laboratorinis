var restful = require('node-restful');
var mongoose = restful.mongoose;

var todoSchema = new mongoose.Schema({
    title: String,
    description: String,
    isDone: Boolean,
    createDate: Date,
    updateDate: Date
});

module.exports = restful.model('todoItem', todoSchema);