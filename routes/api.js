var express = require('express');
var router = express.Router();

var Item = require('../models/todoItem.js');

Item.methods(['get', 'put', 'post', 'delete']);
Item.register(router, '/items');
//https://github.com/baugarten/node-restful/blob/master/lib/handlers.js

module.exports = router;